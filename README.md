#Topological sorting#
## Project created as an assignment for *Introduction to Programming (functional approach)* course ##

### Task ###
Implement a topological sorting as specified in `topol.mli` file. 

In the implementation you can use attached pMap module.

### Grade ###
For this assignment I have achieved the highest possible grade.