(* Aleksander Matusiak *)
(* Sortowanie topologiczne *)

open PMap

exception Cykliczne

(* typ grafu będącego mapą *)
type 'a graph = ('a, 'a list) t

(* kolejka LIFO, moduł skopiowany z wykładu *)
module Q = 
  struct
    exception EmptyQueue
    type 'a queue = 'a list
    let empty = []
    let is_empty q = q = []
    let insert q x = x::q
    let front q =
      if q = [] then raise EmptyQueue
      else List.hd q
    let remove q =
      if q = [] then raise EmptyQueue
      else List.tl q
  end

(* procedura sortująca topologicznie *)
let topol ll = 
  let g = (* stworzenie mapy incydencji *)
    ref (List.fold_left
      (fun acc (a, l) -> add a l acc)
      empty
      ll )
(* stworzenie referencji do mapy, czy wierzchołki odwiedzone,
   0 - nieodwiedzony, 1 - program wszedł do niego, 
   2 - program wyszedł z niego *)
  and visited = 
    ref(List.fold_left
      (fun acc (a,l) ->
	let acc = add a 0 acc in
	List.fold_left
	  (fun ak el -> add el 0 ak)
	  acc
	  l
      )
      empty
      ll) 
  and res = ref [] in (* wynik *)
  let walk x = (* przechodzenie grafu od zadanego wierzchołka *)
    let q = ref Q.empty
    and jakies = ref false
    in let wychodze v = (* procedura wychodząca z wierzchołka *)
      visited := add v 2 !visited;
      res := v::(!res);
      q := Q.remove !q
    in
    begin
      q := Q.insert !q x;
      while not (Q.is_empty !q) do
	let v = Q.front !q in
   (* wychodzę z wierzchołka już przetworzonego *)
	if find v !visited = 1 then begin 
	  wychodze v
	end else if find v !visited = 0 then begin (* wchodzę do wierzchołka *)
	  visited := add v 1 !visited;
   (* zmienna do pamiętania, czy coś dorzuciliśmy na kolejkę *)
	  jakies := false; 
	  if mem v !g then begin
	    List.iter
	      (fun y ->
		let czy = find y !visited in
		if czy = 1 then raise Cykliczne
		else if czy = 0 then begin
		  q := Q.insert !q y;
		  jakies := true
		end)
	      (find v !g);
	    if not !jakies then (* jeśli nic nie dorzuciliśmy do kolejki *)
	      wychodze v
	  end else begin (* jeśli nie ma żadnych krawędzi wychodzących *)
	    wychodze v
	  end
	end else begin (* usunięcie z kolejki już przetworzonego wierzchołka *)
	  q := Q.remove !q
	end
      done 
    end in
  iter (* przeglądanie możliwych wierzchołków starowych *)
    (fun v _ -> if find v !visited = 0 then
      walk v )
    !g;
  !res
  
